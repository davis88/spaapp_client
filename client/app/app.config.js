(function () {
    angular
        .module("SpaApp")
        .config(SpaAppConfig);
    SpaAppConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function SpaAppConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("currentBookings",{
                url : '/currentBookings',
                views: {
                    // 'menu': {
                    // templateUrl: 'app/menu/menu.html',
                    // controller: 'MenuCtrl as ctrl',
                    // },
                    "nav": {
                        templateUrl: "../app/protected/navigation.html",
                        // controller: 'MyAccountCtrl as ctrl',
                    },
                    'content': {
                        templateUrl: "app/view/home.html",
                        controller : 'BookingController as ctrl',
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
            })
            .state("newBooking", {
                url: "/newBooking",
                views: {
                    // 'menu': {
                    // templateUrl: 'app/menu/menu.html',
                    // controller: 'MenuCtrl as ctrl',
                    // },
                    "nav": {
                        templateUrl: "../app/protected/navigation.html",
                        // controller: 'MyAccountCtrl as ctrl',
                    },
                    'content': {
                        templateUrl: "app/view/addBooking.html",
                        controller : 'AddBookingCtrl as ctrl',
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
            })
            .state("SignIn", {
                url: "/signIn",
                views: {
                    "content": {
                        templateUrl: "../app/users/login.html",
                        controller: 'LoginCtrl as ctrl',
                    }
                },
            })
            .state("SignUp", {
                url: "/signUp",
                views: {
                    "content": {
                        templateUrl: "../app/users/register.html",
                        controller: 'RegisterCtrl as ctrl',
                    }
                },
                
            })
            .state("ResetPassword", {
                url: "/ResetPassword",
                views: {
                    "content": {
                        templateUrl: "../app/users/reset-password.html",
                        controller: 'ResetPasswordCtrl as ctrl',
                    }
                },
            
            })
            .state("ChangeNewpassword", {
                url: "/changeNewpassword?token",
                views: {
                    "content": {
                        templateUrl: "../app/users/change-new-password.html",
                        controller: 'ChangeNewPasswordCtrl',
                    }
                },
                
            })
            .state("MyAccount", {
                url: "/MyAccount",
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html",
                        // controller: 'MyAccountCtrl as ctrl',
                    },
                    "content": {
                        templateUrl: "../app/protected/profile.html",
                        // controller: 'MyAccountCtrl as ctrl',
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                
            })
            .state("ChangePassword", {
                url: "/ChangePassword",
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html",
                        // controller: 'ChangePasswordCtrl as ctrl',
                    },
                    "content": {
                        templateUrl: "../app/protected/changePassword.html",
                        // controller: 'ChangePasswordCtrl as ctrl',
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                
            })
            .state('spaApp', {
                url: '/spaApp',
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html",
                        // controller: 'ChangePasswordCtrl as ctrl',
                    },
                    "content": {
                        templateUrl: "../app/protected/calander.html",
                        // controller: 'PostListCtrl as ctrl',
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                
            })
            .state('profile', {
                url: '/profile',
                views: {
                    "nav": {
                        templateUrl: "../app/protected/navigation.html",
                        // controller: 'ProfileCtrl as ctrl',
                    },
                    "content": {
                        templateUrl: "../app/protected/profile.html",
                        // controller: 'ProfileCtrl as ctrl',
                    }
                },
                resolve: {
                    authenticated: function (AuthFactory) {
                        console.log("authenticated ?");
                        console.log(AuthFactory.isLoggedIn());
                        return AuthFactory.isLoggedIn();
                    }
                },
                
            })
            .state('back', {
                url: '/back',
                templateUrl: './app/users/login.html',
                // controller: 'PostListCtrl',
                // controllerAs: 'ctrl'
            })

        $urlRouterProvider.otherwise("/signIn");


    }
})();
