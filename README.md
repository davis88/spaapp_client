// windows
// set AWS_ACCESS_KEY_ID=
// set AWS_SECRET_ACCESS_KEY=

// mac or linux
// export AWS_ACCESS_KEY_ID=
// export AWS_SECRET_ACCESS_KEY=

* export NODE_ENV=production
* export MYSQL_DATABASE_URL=mysql://root:password@123@localhost/wedding_gram?reconnect=true
* forever start --minUptime 1234 --spinSleepTime 3421 ~/spaApp-app/server/app.js > ~/logs/output.log &
* Test