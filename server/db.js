const SequelizeModels = require("sequelize-models");
var moment = require('moment');
var offset = moment().utcOffset();
var finalOffset = ''.concat(offset < 0 ? "-" : "+",moment(''.concat(Math.abs(offset/60),Math.abs(offset%60) < 10 ? "0" : "",Math.abs(offset%60)),"hmm").format("HH:mm"));


var seqModels = new SequelizeModels({
  // Database connection options
  connection: {
    host: "127.0.0.1",
    dialect: "mysql",
    username: "root",
    schema: "spa",
    password: "88YouYou",
    dialectOptions:{
      useUTC:false,
      // dateStrings: true,

    //   typeCast: function (field, next) { // for reading from database
    //     if (field.type === 'DATETIME') {
    //     return field.string()
    //     }
    //     return next()
    // },
    },
  },

  // Models loading options
  models: {
    autoLoad: true,
    path: "models"
  },

  // Sequelize options passed directly to Sequelize constructor
  sequelizeOptions: {
    define: {
      freezeTableName: true,
      underscored: true,
      timestamps: false,
      // timezone: '-16:00',
      // timezone: '+08:00',
      
    }
  }
});

// TODO: LESSON LEARNT
// async function getSchema() {
//     const schema = await seqModels.getSchema();
//     return schema;
// }
// module.exports = getSchema()


module.exports = seqModels.getSchema()