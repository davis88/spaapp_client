var express = require("express");
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require("express-session");
var passport = require("passport");
var flash = require('connect-flash');

var config = require("./config");
var db =  require('./db.js')

var app = express();

app.use(flash());
app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// Initialize session
app.use(session({
    secret: "spa-app-secret",
    resave: false,
    saveUninitialized: true
}));

app.use(function(req, res, next) {
    var allowedOrigins = ['http://localhost:3000', 'http://ec2-18-221-241-215.us-east-2.compute.amazonaws.com'];
    var origin = req.headers.origin;
    if(allowedOrigins.indexOf(origin) > -1){
         res.setHeader('Access-Control-Allow-Origin', origin);
    }
    //res.header('Access-Control-Allow-Origin', 'http://127.0.0.1:8020');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    res.header('Access-Control-Allow-Credentials', true);
    
    return next();
  });

var cors = require('cors')
app.use(cors())


//Initialize passport
app.use(passport.initialize());
app.use(passport.session());

db.then(schema => {
    
        var Models = schema.models
        console.log(Models)
    
        // require('./auth')(app, passport, Models);
        // require('./routes')(app, passport, Models);
    
        require('./auth')(app, passport,Models);
        require('./routes')(app, passport,Models);

        app.listen(config.port, function () {
            console.log("Server running at http://localhost:" + config.port);
        });
})


// app.listen(config.port, function () {
//     console.log("Server running at http://localhost:" + config.port);
// });
