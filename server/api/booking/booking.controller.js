module.exports = function (Models) {
    var Booking = Models.Booking;
    // var Customer = Models.Customer;
    var Staff = Models.Staff;
    var Service = Models.Service;
    var moment = require('moment-timezone')

    get = function (req, res) {
        Booking
            .findById(req.params.id)
            .then(function (booking) {

                if (!booking) {
                    handler404(res);
                }

                res.json(booking);
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    getAllCurrentBookings = function (req, res) {
        var user_id = req.params.user_id
        Booking
            .findAll({
                where: { user_id: user_id },
                include: [
                    { model: Service},
                    { model: Staff}
                ]
            })
            .then(function (bookings) {
                console.log(bookings)
                res.json(bookings);
            })
            .catch(function (err) {
                handleErr(res, err);
            });
                
            };

    getCurrentBookings = function (req, res) {

        var user_id = req.params.user_id
        var keyword = req.query.keyword;
        var sortby = req.query.sortby;
        var itemsPerPage = parseInt(req.query.itemsPerPage);
        var currentPage = parseInt(req.query.currentPage);
        var offset = (currentPage - 1) * itemsPerPage;
        if(sortby == 'null'){
            console.log("sortby is null");
            sortby = "ASC";
        }
        console.log("BACKEDN>.")
    
        var whereClause = { 
            offset: offset,
            limit: itemsPerPage,
            // order: [['start_time', sortby]], 
            where: { user_id: user_id },
            include: [
                { model: Service},
                { model: Staff}
            ]
        };

        console.log(whereClause);
        Booking.findAndCountAll(whereClause).then((results)=>{
            console.log(results)
            res.status(200).json(results);
        }).catch((err)=>{
            console.log(err);
            handleErr(res, err);
        });    


        // var user_id = req.params.user_id
        // Booking
        //     .findAll({
        //         where: { user_id: user_id },
        //         include: [
        //             { model: Service},
        //             { model: Staff}
        //         ]
        //     })
        //     .then(function (bookings) {
        //         res.json(bookings);
        //     })
        //     .catch(function (err) {
        //         handleErr(res, err);
        //     });
        
    };

    list = function (req, res) {
        Booking
            .findAll()
            .then(function (bookings) {
                res.json(bookings);
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    create = function (req, res) {
        var user_id = req.body.user_id;

        var staff_id = req.body.selectedStaff;

        // res = moment(req.body.timeslot).tz('Asia/Singapore')
        
        // console.log("DATE>...")
        // console.log(res)

        var Singapore  = moment.tz(req.body.timeslot, "Asia/Singapore")
        console.log(Singapore.format())

        // var m_start_time = moment(req.body.timeslot)
        // m_start_time.subtract({ hours: 12})


        var data = {
            staff_id: req.body.selectedStaff,
            service_id: req.body.selectedService,
            user_id: user_id,
            date: new Date(moment(req.body.timeslot).toString() + 'UTC'),
            start_time: new Date(moment(req.body.timeslot).toString() + 'UTC'),
            time_slot: new Date(moment(req.body.timeslot).toString() + 'UTC')
        }

        Service.findById(req.body.selectedService).then((service) =>{
            var duration = service.duration
            var time_string = req.body.timeslot
            console.log("GOT IN SERVER SIDE")
            console.log(time_string)

            var req_start_time_slot = new Date(moment(time_string).toString() + 'UTC')
            var req_end_time_slot = new Date(moment(time_string).add({m:duration}).toString() + 'UTC')

            console.log ("req_start_time_slot,req_end_time_slot")
            console.log (req_start_time_slot,req_end_time_slot)
            // target.isBetween(s,e)

            // var my_trimmerd_date = new Date(moment(req.body.timeslot).toString() + 'UTC')
            // my_trimmerd_date = new Date(my_trimmerd_date.setHours(0,0,0,0));

            Booking.findAll({
                            where: {
                                staff_id: staff_id,
                                // date: my_trimmerd_date,
                                start_time: {
                                    $between: [req_start_time_slot, req_end_time_slot]
                                }
                            }
                        })
                        .then(function (results) {
                            
                            if (results.length != 0 ){
                                console.log("SOME BOOKINGS FOUND....")
                                console.log(results)
                                handleErr(res, null);
                            } else {
                                console.log("NO BOOKINGS FOUND.. in that range")

                                Booking
                                .create(data)
                                .then(function (booking) {
                                    res.json(booking);
                                })
                                .catch(function (err) {
                                    handleErr(res, err);
                                });

                            }
                            
                            // for (const index in results) {
                            //     var bk = results[index]
                            //     console.log("OLD => "+ bk.start_time)

                            //     var start_time = bk.start_time
                                
                            //     var timeZoneFromDB = -8.00; //time zone value from database
                            //     //get the timezone offset from local time in minutes
                            //     var tzDifference = timeZoneFromDB * 60 + start_time.getTimezoneOffset();
                            //     //convert the offset to milliseconds, add to targetTime, and make a new Date
                            //     var offsetTime = new Date(start_time.getTime() + tzDifference * 60 * 1000);
                            //     start_time = offsetTime
                            //     start_time.setDate(start_time.getDate() + 1)                  
                            //     console.log("NEW => "+start_time)


                            // }
                            
                        })
                        .catch(function (err) {
                            // handleErr(res, err);
                            console.log(err)
                            console.log("NO BOOKINGS FOUND.... SO AND AN ERROR")
                        });


        }).catch(function (err) {
            handleErr(res, err);
        });

        


       
    };

    update = function (req, res) {
        Booking
            .findById(req.params.id)
            .then(function (booking) {

                if (!booking) {
                    handler404(res);
                }

                booking.update(req.body).then(function (booking) {
                    res.json(booking);
                })
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    remove = function (req, res) {
        Booking
            .findById(req.params.id)
            .then(function (booking) {
                if (!booking) {
                    handler404(res);
                }
                booking.destroy({ force: true })
                res.json(booking);
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    function handleErr(res) {
        handleErr(res, null);
    }


    function handleErr(res, err) {
        console.log(err);
        res
            .status(500)
            .json({
                error: true
            });
    }

    function handler404(res) {
        res
            .status(404)
            .json({ message: "Booking not found!" });
    }

    function returnResults(results, res) {
        res.send(results);
    }

    return {
        list : list,
        create: create,
        get : get,     
        getCurrentBookings: getCurrentBookings,
        getAllCurrentBookings: getAllCurrentBookings,   
        update: update,
        remove: remove,
    }

}