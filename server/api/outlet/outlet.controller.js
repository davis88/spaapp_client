module.exports = function (Models) {
    var Outlet = Models.Outlet;

    get = function (req, res) {
        Outlet
            .findById(req.params.id)
            .then(function (outlet) {

                if (!outlet) {
                    handler404(res);
                }

                res.json(outlet);
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    list = function (req, res) {
        Outlet
            .findAll()
            .then(function (users) {
                res.json(users);
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    create = function (req, res) {
        Outlet
            .create(req.body)
            .then(function (outlet) {
                res.json(outlet);
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    update = function (req, res) {
        Outlet
            .findById(req.params.id)
            .then(function (outlet) {

                if (!outlet) {
                    handler404(res);
                }

                outlet.update(req.body).then(function (outlet) {
                    res.json(outlet);
                })
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    remove = function (req, res) {
        Outlet
            .findById(req.params.id)
            .then(function (outlet) {
                if (!outlet) {
                    handler404(res);
                }
                
                outlet.destroy({ force: true })
                res.json(outlet);
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    function handleErr(res) {
        handleErr(res, null);
    }


    function handleErr(res, err) {
        console.log(err);
        res
            .status(500)
            .json({
                error: true
            });
    }

    function handler404(res) {
        res
            .status(404)
            .json({ message: "Outlet not found!" });
    }

    function returnResults(results, res) {
        res.send(results);
    }

    return {
        list : list,
        create: create,
        get : get,        
        update: update,
        remove: remove,
    }

}