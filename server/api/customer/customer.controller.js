module.exports = function (Models) {
    var Customer = Models.Customer;

    get = function (req, res) {
        Customer
            .findById(req.params.id)
            .then(function (customer) {

                if (!customer) {
                    handler404(res);
                }

                res.json(customer);
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    getByName = function (req, res) {
        var firstname = req.params.firstname
        
        Customer
            .findOne({
                where:{
                    "firstname":firstname
                }
            }).then(customer => {

                if (!customer) {
                    handler404(res);
                }

                res.json(customer);
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    
    
            

    list = function (req, res) {
        Customer
            .findAll()
            .then(function (users) {
                res.json(users);
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    create = function (req, res) {
        Customer
            .create(req.body)
            .then(function (customer) {
                res.json(customer);
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    update = function (req, res) {
        Customer
            .findById(req.params.id)
            .then(function (customer) {

                if (!customer) {
                    handler404(res);
                }

                customer.update(req.body).then(function (customer) {
                    res.json(customer);
                })

            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    remove = function (req, res) {
        Customer
            .findById(req.params.id)
            .then(function (customer) {
                if (!customer) {
                    handler404(res);
                }

                customer.destroy({ force: true })
                res.json(customer);
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    function handleErr(res) {
        handleErr(res, null);
    }


    function handleErr(res, err) {
        console.log(err);
        res
            .status(500)
            .json({
                error: true
            });
    }

    function handler404(res) {
        res
            .status(404)
            .json({ message: "Customer not found!" });
    }

    function returnResults(results, res) {
        res.send(results);
    }

    return {
        list : list,
        create: create,
        get : get,   
        getByName: getByName,     
        update: update,
        remove: remove,
    }

}