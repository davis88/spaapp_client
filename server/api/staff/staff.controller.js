module.exports = function (Models) {
    var Staff = Models.Staff;
    var Outlet = Models.Outlet;
    var Booking = Models.Booking;
    var Service = Models.Service;
    var User = Models.User;
    var bcrypt   = require('bcryptjs');

    get = function (req, res) {
        Staff
            .findById(req.params.id)
            .then(function (staff) {

                if (!staff) {
                    handler404(res);
                }

                res.json(staff);
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    list = function (req, res) {
        Staff
            .findAll(
                {
                    include: [
                        { model: Outlet}
                    ]
                })
            .then(function (users) {
                res.json(users);
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };


    


    logintest = function (req, res) {
        
        var username = req.body.username
        var password = req.body.password
    
        console.log("CHECKING USERNAME AND PASSWORD")
        console.log(username + "  " + password)
        if(!password){
            return res.status(500).json({
                success: false,
                err: "Error : password is empty"
            });
        }
        User.findOne({
            where: {
                email: username
            }
        }).then(function(result) {
            if (!result) {
                return res.status(500).json({
                    success: false,
                    err: "Something went wrong 1"
                });
            } else {
                if (bcrypt.compareSync(password, result.password)) {
                    var whereClause = {};
                    whereClause.email = username;
                    User
                        .update({ reset_password_token: null }, { where: whereClause });
                    
                    return res.status(200).json(result);
                } else {
                    return res.status(500).json({
                        success: false,
                        err: "In-correct password/username"
                    });
                }
            }
        }).catch(function(err) {
            return res.status(500).json({
                success: false,
                err: "Something went wrong 2"+ err
            });
        });
    };
    


    listByBooking = function (req, res) {
        var staff_id = req.params.id

        Booking.findAll({
            where: {
                staff_id: staff_id
                // date: my_trimmerd_date,
                // start_time: {
                //     $between: [req_start_time_slot, req_end_time_slot]
                // }
            },
            include: [
                { model: Service}
            ]
        })
        .then(function (results) {
                res.json(results);
            })
        .catch(function (err) {
            handleErr(res, err);
        });
    };

    

    create = function (req, res) {
        Staff
            .create(req.body)
            .then(function (staff) {
                res.json(staff);
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    update = function (req, res) {
        Staff
            .findById(req.params.id)
            .then(function (staff) {

                if (!staff) {
                    handler404(res);
                }

                staff.update(req.body).then(function (staff) {
                    res.json(staff);
                })
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    remove = function (req, res) {
        Staff
            .findById(req.params.id)
            .then(function (staff) {
                if (!staff) {
                    handler404(res);
                }

                staff.destroy({ force: true })
                res.json(staff)
            })
            .catch(function (err) {
                handleErr(res, err);
            });
    };

    function handleErr(res) {
        handleErr(res, null);
    }


    function handleErr(res, err) {
        console.log(err);
        res
            .status(500)
            .json({
                error: true
            });
    }

    function handler404(res) {
        res
            .status(404)
            .json({ message: "Staff not found!" });
    }

    function returnResults(results, res) {
        res.send(results);
    }

    return {
        list : list,
        listByBooking: listByBooking,
        logintest:logintest,
        create: create,
        get : get,        
        update: update,
        remove: remove,
    }

}