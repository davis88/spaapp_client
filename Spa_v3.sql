CREATE DATABASE  IF NOT EXISTS `spa` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `spa`;


-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: localhost    Database: spa
-- ------------------------------------------------------
-- Server version	5.7.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `addon`
--

DROP TABLE IF EXISTS `addon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addon`
--

LOCK TABLES `addon` WRITE;
/*!40000 ALTER TABLE `addon` DISABLE KEYS */;
INSERT INTO `addon` VALUES (1,'addon','ear candling','2017-11-17 15:04:53'),(2,'addon','basic manicure','2017-11-17 15:04:53'),(3,'addon','ear cleansing','2017-11-17 15:04:53'),(4,'addon','facial wash','2017-11-17 15:04:53');
/*!40000 ALTER TABLE `addon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authentication_provider`
--

DROP TABLE IF EXISTS `authentication_provider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authentication_provider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `providerId` varchar(45) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `providerType` varchar(45) DEFAULT NULL,
  `displayName` varchar(45) DEFAULT NULL,
  `profile_photo` blob,
  `createdAt` timestamp NULL DEFAULT NULL,
  `updatedAt` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `auth_provider_customer_idx` (`userId`),
  CONSTRAINT `auth_provider_customer` FOREIGN KEY (`userId`) REFERENCES `customer` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authentication_provider`
--

LOCK TABLES `authentication_provider` WRITE;
/*!40000 ALTER TABLE `authentication_provider` DISABLE KEYS */;
/*!40000 ALTER TABLE `authentication_provider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `booking`
--

DROP TABLE IF EXISTS `booking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) NOT NULL,
  `start_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date` date NOT NULL,
  `time_slot` time NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `booking_customer` (`user_id`),
  KEY `booking_service` (`service_id`),
  KEY `booking_staff` (`staff_id`),
  CONSTRAINT `booking_service` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`),
  CONSTRAINT `booking_staff` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`id`),
  CONSTRAINT `booking_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booking`
--

LOCK TABLES `booking` WRITE;
/*!40000 ALTER TABLE `booking` DISABLE KEYS */;
INSERT INTO `booking` VALUES (67,1,'2017-11-29 14:00:00','2017-11-29','14:00:00','2017-11-26 13:54:26',10,6),(68,4,'2017-12-15 14:30:00','2017-12-15','14:30:00','2017-12-04 12:53:23',13,2),(70,1,'2017-12-16 11:00:00','2017-12-16','11:00:00','2017-12-04 12:55:07',13,6),(71,1,'2017-12-29 13:00:00','2017-12-29','13:00:00','2017-12-07 16:39:15',8,2),(72,1,'2017-12-15 15:00:00','2017-12-15','15:00:00','2017-12-08 18:31:33',14,1),(73,4,'2017-12-22 15:00:00','2017-12-22','15:00:00','2017-12-08 18:35:26',14,2),(74,1,'2017-12-15 15:30:00','2017-12-15','15:30:00','2017-12-11 11:16:22',8,2),(75,2,'2017-12-29 15:30:00','2017-12-29','15:30:00','2017-12-11 14:49:17',16,2);
/*!40000 ALTER TABLE `booking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `booking_addon`
--

DROP TABLE IF EXISTS `booking_addon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `booking_addon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_id` int(11) NOT NULL,
  `addon_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `booking_addon_addon` (`addon_id`),
  KEY `booking_addon_booking` (`booking_id`),
  CONSTRAINT `booking_addon_addon` FOREIGN KEY (`addon_id`) REFERENCES `addon` (`id`),
  CONSTRAINT `booking_addon_booking` FOREIGN KEY (`booking_id`) REFERENCES `booking` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booking_addon`
--

LOCK TABLES `booking_addon` WRITE;
/*!40000 ALTER TABLE `booking_addon` DISABLE KEYS */;
/*!40000 ALTER TABLE `booking_addon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(200) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `contact` varchar(200) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `password` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (1,'davis','lim','davis88@ymail.com','81463615','55 Hume Avenue ','2017-11-20 14:48:19','davis'),(2,'mano','ramesh','thlimdavis@gmail.com\\','93920961','49 Hume Avenue','2017-11-20 14:48:19','mano');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `outlet`
--

DROP TABLE IF EXISTS `outlet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `outlet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `createdAtcustomers` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `outlet`
--

LOCK TABLES `outlet` WRITE;
/*!40000 ALTER TABLE `outlet` DISABLE KEYS */;
INSERT INTO `outlet` VALUES (1,'Marina Bay Sands Spa Outlet','2017-11-25 16:19:54'),(2,'Sentosa Resort & Spa Outlet','2017-11-25 16:19:54');
/*!40000 ALTER TABLE `outlet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service`
--

DROP TABLE IF EXISTS `service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `duration` varchar(200) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service`
--

LOCK TABLES `service` WRITE;
/*!40000 ALTER TABLE `service` DISABLE KEYS */;
INSERT INTO `service` VALUES (1,'bodyMassage','Shiatsu','60','2017-11-17 15:13:35'),(2,'bodyMassage','Shiatsu','90','2017-11-17 15:13:35'),(3,'bodyMassage','Tui Na','60','2017-11-17 15:13:35'),(4,'bodyMassage','Tui Na','90','2017-11-17 15:13:35'),(5,'bodyMassage','Swedish','60','2017-11-17 15:13:35'),(6,'bodyMassage','Swedish','90','2017-11-17 15:13:35'),(7,'footMassage','Oil Essence','60','2017-11-17 15:13:35'),(8,'footMassage','Foot Bath','60','2017-11-17 15:13:35');
/*!40000 ALTER TABLE `service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff`
--

DROP TABLE IF EXISTS `staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `contact` varchar(200) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `profile_url` varchar(1000) DEFAULT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `outlet_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `staff_outlet` (`outlet_id`),
  CONSTRAINT `staff_outlet` FOREIGN KEY (`outlet_id`) REFERENCES `outlet` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff`
--

LOCK TABLES `staff` WRITE;
/*!40000 ALTER TABLE `staff` DISABLE KEYS */;
INSERT INTO `staff` VALUES (1,'athena','athena@spa.com','12345678','Marina Bay Sands Spa Outlet','https://images-cdn.9gag.com/photo/aEGnNeK_700b.jpg','2017-11-25 14:40:36',1),(2,'belinda','belinda@spa.com','87654321','Marina Bay Sands Spa Outlet','http://1.bp.blogspot.com/-xF5kmFjeX9M/ViVSaVz_6nI/AAAAAAAAMAk/r6kY-3t5kHk/s1600/beautiful%2Bindonesian%2BFadhila%2BHananing.jpg','2017-12-07 13:54:05',1),(3,'evelyn','evelyn@spa.com','23456789','Sentosa Resort & Spa Outlet','https://i.imgur.com/6gPo463.jpg','2017-11-25 14:40:36',2),(4,'angela','angela@spa.com','98765432','Sentosa Resort & Spa Outlet','https://d3lp4xedbqa8a5.cloudfront.net/s3/digital-cougar-assets/cosmo/2017/06/27/22903/LureHsu_Main.jpg?width=922&mode=crop&anchor=topcenter&quality=75','2017-11-25 14:40:36',2);
/*!40000 ALTER TABLE `staff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staff_service`
--

DROP TABLE IF EXISTS `staff_service`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `staff_service_service` (`service_id`),
  KEY `staff_service_staff` (`staff_id`),
  CONSTRAINT `staff_service_service` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`),
  CONSTRAINT `staff_service_staff` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staff_service`
--

LOCK TABLES `staff_service` WRITE;
/*!40000 ALTER TABLE `staff_service` DISABLE KEYS */;
INSERT INTO `staff_service` VALUES (1,1,1),(2,1,2),(3,1,3),(4,1,4),(5,1,5),(6,1,6),(7,2,1),(8,2,2),(9,2,3),(10,2,4),(11,3,1),(12,3,2),(13,3,3),(14,3,4),(15,3,7),(16,3,8);
/*!40000 ALTER TABLE `staff_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `addressLine1` varchar(255) DEFAULT NULL,
  `addressLine2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `postcode` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `reset_password_token` varchar(255) DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) DEFAULT NULL,
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) DEFAULT NULL,
  `last_sign_in_ip` varchar(255) DEFAULT NULL,
  `isAdmin` tinyint(1) DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin@spa.com','$2a$08$3djB8.OyZhg/WSlY8spfEOkW9fFAz0JDmG2eB.e3NaSok9UtZQz9a','Admin','Admin','admin@spa.com','79 02 Ayer Rajah crescent','','Singapore','42312','90909090',NULL,NULL,NULL,NULL,NULL,'2017-11-24 11:52:28','2017-11-24 11:52:28',NULL,NULL,1,NULL),(2,'davis@gmail.com','$2a$08$3djB8.OyZhg/WSlY8spfEOkW9fFAz0JDmG2eB.e3NaSok9UtZQz9a','Davis','Lim','davis@gmail.com','79 02 Ayer Rajah Crescent','','Singapore','42312','90909090',NULL,NULL,NULL,NULL,NULL,'2017-11-24 11:52:28','2017-11-24 11:52:28',NULL,NULL,NULL,NULL),(7,'test@test.com','$2a$08$bf/TvrWfa4Ol1cJbFNRL.ualDjK27Wpc5hxXRMOWfZpTxlmQCH5SG','test','test','test@test.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-11-24 15:52:47','2017-11-24 15:52:47',NULL,NULL,NULL,NULL),(8,'thlimdavis@gmail.com',NULL,NULL,NULL,'thlimdavis@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,'davis88@gmail.com','$2a$08$KWMRdo3i7.HF.h2lbd/eXeSMZL0uv2SJ/Lg/6b.5dpAWppPzIgfPK','davis','Lim','davis88@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-11-26 09:51:27','2017-11-26 09:51:27',NULL,NULL,NULL,NULL),(10,'davis88@ymail.com','$2a$08$9966XYzaiiHu/6DWzu0XMeJlug9ANV0RDdPCHDNKNG8g4JzKeB9kC','davis','lim','davis88@ymail.com',NULL,NULL,NULL,NULL,NULL,'$2a$08$Eo/K1r201bHBlN6vnVmZAeWRgoU2TEF.ktwH0yivNO0/3sfGyzodO',NULL,1,NULL,NULL,'2017-11-26 13:52:58','2017-12-04 12:35:49',NULL,NULL,NULL,'2017-12-04 12:35:49'),(12,'thlimdavis',NULL,NULL,NULL,'thlimdavis',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(13,'davis88lim@gmail.com','$2a$08$ygP6Sqhiw/WzMFMZD6y/hOquA.6lwLLPXtVQKCcwDv90HuiUy4hFS','davis','lim','davis88lim@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-12-04 12:51:53','2017-12-04 12:51:53',NULL,NULL,NULL,NULL),(14,'user@user.com','$2a$08$4qOG7rBqOGlz9K89Dj1of.C9YwiYb0C4fx4PyOnzLJfdrbzHu11Pi','user','user','user@user.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-12-08 18:30:57','2017-12-08 18:30:57',NULL,NULL,NULL,NULL),(15,'demo@spa.com','$2a$08$3aZiS6gc405RJzZquc7GGuM/1uWATJQZEsQ60oHs.zHKJHwKNadO6','demo','demo','demo@spa.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-12-11 13:26:36','2017-12-11 13:26:36',NULL,NULL,NULL,NULL),(16,'newuser@spa.com','$2a$08$9KJvLV8M9aZ0EeYGp4FN5ORjc/QiD4ffxZKU6J90UMwNd38I53UgG','newuser','newuser','newuser@spa.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-12-11 14:48:48','2017-12-11 14:48:48',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-11 23:12:13



'1', 'athena', 'athena@spa.com', '89898989', 'Marina Bay Sands Spa', 'https://images-cdn.9gag.com/photo/aEGnNeK_700b.jpg', '2017-11-23 21:42:13', '1'
'2', 'belinda', 'belinda@spa.com', '92789821', 'Marina Bay Sands Spa', 'https://s-media-cache-ak0.pinimg.com/originals/bd/a6/0b/bda60be5f58d2ab3b375695682a9aa9d.jpg', '2017-11-23 21:42:13', '1'
'3', 'evelyn', 'evelyn@spa.com', '67676767', 'Sentosa Resort & Spa', 'http://sguru.org/wp-content/uploads/2017/04/facebook-profile-photo-for-beautiful-girls-face-8.jpg', '2017-11-23 21:42:13', '2'
'4', 'angela', 'angela@spa.com', '78787878', 'Sentosa Resort & Spa', 'https://i.imgur.com/6gPo463.jpg', '2017-11-23 21:42:13', '2'
'5', 'francesca', 'francesca@spa.com', '31383138', 'Marina Bay Sands Spa', 'https://d25tv1xepz39hi.cloudfront.net/images/103_1.jpg', '2017-11-23 21:42:13', '1'
'6', 'celine', 'celine@spa.com', '38313831', 'Sentosa Resort & Spa', 'https://www.russianfreedating.com/user_images/0/165/723.jpg', '2017-11-23 21:42:13', '2'
'7', 'chanel', 'chanel@spa.com', '31833183', 'Marina Bay Sands Spa', 'https://d3lp4xedbqa8a5.cloudfront.net/s3/digital-cougar-assets/cosmo/2017/06/27/22903/LureHsu_Main.jpg?width=922&mode=crop&anchor=topcenter&quality=75', '2017-11-23 21:42:13', '1'
'8', 'priscillia', 'priscillia@spa.com', '83318331', 'Sentosa Resort & Spa', 'http://1.bp.blogspot.com/-xF5kmFjeX9M/ViVSaVz_6nI/AAAAAAAAMAk/r6kY-3t5kHk/s1600/beautiful%2Bindonesian%2BFadhila%2BHananing.jpg', '2017-11-23 21:42:13', '2'
